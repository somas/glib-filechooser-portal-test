# main.py
#
# Copyright 2020 Manuel Genovés
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import gi

gi.require_version('Gtk', '3.0')

from gi.repository import Gtk, Gio

from .window import GlibFilechooserPortalTestWindow


class Application(Gtk.Application):
    def __init__(self):
        super().__init__(application_id='es.manugen.glib-filechooserportal-test',
                         flags=Gio.ApplicationFlags.FLAGS_NONE)

    def do_startup(self):
        Gtk.Application.do_startup(self)

        action = Gio.SimpleAction.new("open", None)
        action.connect("activate", self.on_open)
        self.add_action(action)

        action = Gio.SimpleAction.new("save", None)
        action.connect("activate", self.on_save)
        self.add_action(action)

    def do_activate(self):
        self.win = self.props.active_window
        if not self.win:
            self.win = GlibFilechooserPortalTestWindow(application=self)
        self.win.present()

    def on_open(self, _action, _value):
        self.win.open()

    def on_save(self, _action, _value):
        self.win.save()


def main(version):
    app = Application()
    return app.run(sys.argv)
