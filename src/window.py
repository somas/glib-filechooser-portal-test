# window.py
#
# Copyright 2020 Manuel Genovés
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk, Gio, GLib


@Gtk.Template(resource_path='/es/manugen/glib-filechooserportal-test/window.ui')
class GlibFilechooserPortalTestWindow(Gtk.ApplicationWindow):
    __gtype_name__ = 'GlibFilechooserPortalTestWindow'

    text_editor = Gtk.Template.Child()
    open_button = Gtk.Template.Child()
    save_button = Gtk.Template.Child()
    header_bar  = Gtk.Template.Child()
    log_textview = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.text_buffer = self.text_editor.get_buffer()

        self.log_buffer = self.log_textview.get_buffer()

    def open(self):
        plaintext_filter = Gtk.FileFilter.new()
        plaintext_filter.add_mime_type('text/plain')
        plaintext_filter.set_name(_('Plain Text Files'))

        filechooser = Gtk.FileChooserNative.new(
            "Open a text file",
            self,
            Gtk.FileChooserAction.OPEN,
            "Open",
            "Cancel"
        )

        filechooser.set_local_only(False)
        filechooser.add_filter(plaintext_filter)
        response = filechooser.run()
        if response == Gtk.ResponseType.ACCEPT:
            self.file = filechooser.get_file()

            self.load_file(self.file)
            self.update_headerbar(self.file)

            filechooser.destroy()

        elif response == Gtk.ResponseType.CANCEL:
            filechooser.destroy()

    def load_file(self, file):

        file.load_contents_async(None, self._load_contents_cb, None)

    def _load_contents_cb(self, file, result, user_data=None):
        try:
            _success, contents, _etag = file.load_contents_finish(result)
        except GLib.GError as error:
            print(str(error.message))
            return

        decoded = contents.decode('UTF-8')

        self.text_buffer.set_text(decoded)

    def save(self):

        start_iter = self.text_buffer.get_start_iter()
        end_iter = self.text_buffer.get_end_iter()
        text = self.text_buffer.get_text(start_iter, end_iter, False)

        encoded_text = GLib.Bytes.new(text.encode("UTF-8"))

        self.file.replace_contents_bytes_async(
            encoded_text,
            etag=None,
            make_backup=None,
            flags=Gio.FileCreateFlags.NONE,
            cancellable=None,
            callback=self._replace_contents_cb,
            user_data=None)

    def _replace_contents_cb(self, gfile, result, _user_data=None):
        try:
            success, _etag = gfile.replace_contents_finish(result)
        except GLib.GError as error:
            self.log_buffer.insert_at_cursor(str(error.message) + "\n")
            return

        return success

    def update_headerbar(self, file):
        file_info = file.query_info("standard", Gio.FileQueryInfoFlags.NONE,
                                    None)
        title = file_info.get_attribute_as_string("standard::display-name")

        self.header_bar.set_title(title)


